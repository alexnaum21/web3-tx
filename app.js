var express = require('express');
var bodyParser = require('body-parser');
var app = express();

var contractAbi = require('./abi');
var contractAddress = '0x070DD6eb24D375669fc0DEfA253501A3158BEB2B';
var addressAcc = '0x1ca815abdd308caf6478d5e80bfc11a6556ce0ed';
var addressPass = '';
var Web3 = require('web3');
var bcrypt = require('bcryptjs');

const web3 = new Web3(new Web3.providers.HttpProvider("https://kovan.infura.io/fd28b54b765d41c8d352d092576bb125"));


//var contractInstance = MyContract.at(contractAddress);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.get('/tx/:address', function (req, res) {


});


app.post('/white-list', function (req, res) {
    var body = req.body;
    if (!body.address) return res.status(400).json({message: 'Address required'});
    bcrypt.compare(body.passphrase, '$2a$10$dMQZe6jKDEGTQK40ot0ZFuE/Maul1E7OQ3/S0WZtdnKbMAS6ydWMq', function (err, isMatch) {

        if (err) {
            return res.status(500).json({status: err});
        } else if (!isMatch) return res.status(403).json({});
        else {
            web3.personal.unlockAccount(addressAcc, addressPass, 30000);
            var contractInstance = new web3.eth.Contract(contractAbi, contractAddress);
            contractInstance
                .methods
                .registerMember(body.address, !body.status === false, body.bonus ? body.bonus : 0, body.preOredered ? body.preOredered : 0)
                .send({from: addressAcc},
                    function (error, transactionHash) {
                        console.log('registered', error, transactionHash);
                        if (err) {
                            return res.status(500).json({status: error});
                        } else {
                            return res.status(201).json({tr: transactionHash});

                        }
                    });
        }

    });


});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});

